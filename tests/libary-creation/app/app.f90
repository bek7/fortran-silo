program library_test
        use lib_f
        implicit none
        include "lib.inc"

        integer :: int_return

        int_return = test_f()
        if (int_return == 5 .and. CONSTANT_F == 5) then
                print *, "  Fortran Test Success!"
        else
                print *, "XXFortran Test Fail"
        end if

        int_return = test_c()
        if (int_return == 7 .and. CONSTANT_C == 7) then
                print *, "  C Test Success!"
        else
                print *, "XXC Test Fail"
        end if

end program library_test
