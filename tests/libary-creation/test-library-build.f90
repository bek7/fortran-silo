program library_test
        implicit none
        include "fortransilo-reader.inc"

        integer :: int_return
        integer, parameter :: int_expected = 15

        print *, "Hello World!", tst_constant

        int_return = lib_test_()

        if (int_expected == int_return)then
                print *, "Function Test Success!"
        else
                print *, "Function Test Fail"
        end if

end program library_test
