module testdata_in

    ! Quadmesh 1: Single Precision, 3D
    character(len=:), pointer :: in_d3_qmesh_xname
    character(len=:), pointer :: in_d3_qmesh_yname
    character(len=:), pointer :: in_d3_qmesh_zname
    real(kind=8), pointer :: in_d3_qmesh_x(:) => null()
    real(kind=8), pointer :: in_d3_qmesh_y(:) => null()
    real(kind=8), pointer :: in_d3_qmesh_z(:) => null()
    integer, pointer :: in_d3_qmesh_dims(:) => null()
    integer :: in_d3_qmesh_ndims
    integer :: in_d3_qmesh_datatype
    integer :: in_d3_qmesh_coordtype
    integer :: in_d3_qmesh_optlist

    ! Quadmesh 1 options
    integer :: in_d3_qmesh_groupno
    integer :: in_d3_qmesh_cycle
    integer :: in_d3_qmesh_coordsys
    integer :: in_d3_qmesh_majororder
    integer :: in_d3_qmesh_facetype
    integer :: in_d3_qmesh_planar
    real(kind=4) :: in_d3_qmesh_time
    real(kind=8) :: in_d3_qmesh_dtime
    character(len=:), allocatable :: in_d3_qmesh_xlabel
    integer :: in_d3_qmesh_nspace
    integer :: in_d3_qmesh_origin
    integer, pointer :: in_d3_qmesh_baseindex(:) => null()

    !    !Quadvar 1: Double Precision, 3D
    character(len=:), pointer :: in_d3_qvar_name
    character(len=:), pointer :: in_d3_qvar_meshname
    integer :: in_d3_qvar_nvars
    character(len=:), pointer :: in_d3_qvar_varnames(:)
    real(kind=8), pointer :: in_d3_qvar_vars(:,:,:) => null()
    integer, pointer :: in_d3_qvar_dims(:) => null()
    integer :: in_d3_qvar_ndims
    character, pointer :: in_d3_qvar_mixvars
    integer, pointer :: in_d3_qvar_mixlen(:) => null()
    integer :: in_d3_qvar_datatype
    integer :: in_d3_qvar_centering
     integer :: in_d3_qvar_optlist

    ! Quadvar options
    integer :: in_d3_qvar_coordsys
    integer :: in_d3_qvar_cycle
    integer :: in_d3_qvar_facetype
    integer :: in_d3_qvar_majororder
    integer :: in_d3_Qvar_origin
    real(kind=4) :: in_d3_Qvar_time
    real(kind=8) :: in_d3_qvar_dtime
    integer :: in_d3_Qvar_usespecmf
    integer :: in_d3_qvar_asciilabel
    integer :: in_d3_qvar_conserved
    integer :: in_d3_qvar_extensive
    integer :: in_d3_qvar_guihide

    ! Multimesh
    character(len=13) :: in_mmesh_name = "tst_multimesh"
    integer :: in_nmmesh_mesh
    character(len=:), pointer :: in_mmesh_meshnames(:)
    integer, pointer :: in_mmesh_meshtypes(:) => null()
    integer :: in_mmesh_optlist

    ! Multimesh Options
    integer :: in_mmesh_blockorigin
    integer :: in_mmesh_cycle
    real(kind=4) :: in_mmesh_time
    real(kind=8) :: in_mmesh_dtime
    integer :: in_mmesh_extentssize
    real(kind=4), pointer :: in_mmesh_extents  ! TODO: Look into testing this
    integer, pointer :: in_mmesh_zonecounts    ! TODO: Testing
    integer, pointer :: in_mmesh_hasexternal   ! TODO: test
    integer :: in_mmesh_guihide
    ! TODO: MRGTREE Name
    integer :: in_mmesh_tvconnectivity
    integer :: in_mmesh_disjoint
    integer :: in_mmesh_topodim
    integer :: in_mmesh_mbblocktype
    ! TODO: MB File Namescheme
    ! TODO: BM Block Namescheme
    integer, pointer :: in_mmesh_emptylist     ! TODO: Test
    integer :: in_mmesh_mbemptycount

    ! Multivar
    character(len=13) :: in_mvar_name = "tst_multivar"
    integer :: in_mvar_nvar
    character(len=:), pointer :: in_mvar_varnames(:)
    integer, pointer :: in_mvar_vartypes(:) => null()
    integer :: in_mvar_optlist

    ! Multivar Options
    integer :: in_mvar_blockorigin
    integer :: in_mvar_cycle
    real(kind=4) :: in_mvar_time
    real(kind=8) :: in_mvar_dtime
    integer :: in_mvar_guihide
    integer :: in_mvar_extentssize
    real(kind=4), pointer :: in_mvar_extents   ! TODO
    ! TODO: MMesh name
    integer :: in_mvar_tensorrank
    ! TODO: Region PNames
    integer :: in_mvar_conserved
    integer :: in_mvar_extensive
    integer :: in_mvar_mbblocktype
    ! TODO: MB File NS
    ! TODO: MB Block NS
    integer, pointer :: in_mvar_emptylist
    integer :: in_mvar_emptycount
    real(kind=8) :: in_mvar_missing


end module testdata_in
