module unit_test
    use testdata_out    ! Data to be written in unit tests
    use testdata_in     ! Data to be read in unit tests
    implicit none

    ! All tests
    integer :: tests = 0
    integer :: pass = 0
    integer :: fail = 0

    ! Integer tests
    integer :: int_test = 0
    integer :: int_pass = 0
    integer :: int_fail = 0

    ! Real SP tests
    integer :: rls_test = 0
    integer :: rls_pass = 0
    integer :: rls_fail = 0

    ! Real DP tests
    integer :: rld_test = 0
    integer :: rld_pass = 0
    integer :: rld_fail = 0

    ! Integer array tests
    integer :: iar_test = 0
    integer :: iar_pass = 0
    integer :: iar_fail = 0

    integer :: d3_test = 0
    integer :: d3_pass = 0
    integer :: d3_fail = 0

    ! Real SP array tests
    integer :: rsa_test = 0
    integer :: rsa_pass = 0
    integer :: rsa_fail = 0

    ! Real DP array tests
    integer :: rda_test = 0
    integer :: rda_pass = 0
    integer :: rda_fail = 0

    ! String tests
    integer :: str_test = 0
    integer :: str_pass = 0
    integer :: str_fail = 0

    interface compare
        module procedure int_compare
        module procedure reals_compare
        module procedure reald_compare
        module procedure int_array_compare
        module procedure real3d_array_compare
        module procedure reals_array_compare
        module procedure reald_array_compare
        module procedure string_ptr_compare
    end interface compare

    contains

        ! Compare integers
        subroutine int_compare(expected, result, topic)
            implicit none

            integer, intent(in) :: expected
            integer, intent(in) :: result
            character(len=*), intent(in) :: topic

            if (expected .EQ. result) then
                tests = tests + 1
                pass = pass + 1
                int_test = int_test + 1
                int_pass = int_pass + 1
                print *, "  PASS   ", topic, result
            else
                tests = tests + 1
                fail = fail + 1
                int_test = int_test + 1
                int_fail = int_fail + 1
                print *, "**FAIL** ", topic, " - Expected: ", expected, " Result: ", result
            end if
        end subroutine int_compare

        ! Compare Single Precision Real
        subroutine reals_compare(expected, result, topic)
            implicit none

            real(kind=4), intent(in) :: expected
            real(kind=4), intent(in) :: result
            character(len=*), intent(in) :: topic

            if (expected .EQ. result) then
                tests = tests + 1
                pass = pass + 1
                rls_test = rls_test + 1
                rls_pass = rls_pass + 1
                print *, "  PASS   ", topic, result
            else
                tests = tests + 1
                fail = fail + 1
                rls_test = rls_test + 1
                rls_fail = rls_fail + 1
                print *, "**FAIL** ", topic, " - Expected: ", expected, " Result: ", result
            end if

        end subroutine reals_compare

        ! Compare Double Precision Real
        subroutine reald_compare(expected, result, topic)
            implicit none

            real(kind=8), intent(in) :: expected
            real(kind=8), intent(in) :: result
            character(len=*), intent(in) :: topic

            if (expected .EQ. result) then
                tests = tests + 1
                pass = pass + 1
                rld_test = rld_test + 1
                rld_pass = rld_pass + 1
                print *, "  PASS   ", topic, result
            else
                tests = tests + 1
                fail = fail + 1
                rld_test = rld_test + 1
                rld_fail = rld_fail + 1
                print *, "**FAIL** ", topic, " - Expected: ", expected, " Result: ", result
            end if

        end subroutine reald_compare

        ! Compare Int arrays
        subroutine int_array_compare(expected, result, topic)
            implicit none

            integer, intent(in) :: expected(:)
            integer, intent(in) :: result(:)
            character(len=*), intent(in) :: topic
            integer :: l_ex
            integer :: l_rs
            integer :: iter

            l_ex = size(expected)
            l_rs = size(result)

            if(l_ex /= l_rs) then
                tests = tests + 1
                fail = fail + 1
                iar_test = iar_test + 1
                iar_fail = iar_fail + 1
                print *, "**FAIL** ", topic, " - mismatched length - Expected: ", l_ex, " Result: ", l_rs
                print *, "Expected: ", expected
                print *, "Result:   ", result
                return
            else
                do iter = 1, l_rs, 1
                    if(expected(iter) /= result(iter)) then
                        tests = tests + 1
                        fail = fail + 1
                        iar_test = iar_test + 1
                        iar_fail = iar_fail + 1
                        print *, "**FAIL** ", topic, " - Expected: ", expected(iter), " Result: ", result(iter),&
                                " Index: ", iter
                        print *, "Expected: ", expected
                        print *, "Result:   ", result
                        return
                    end if
                end do
            end if

            tests = tests + 1
            pass = pass + 1
            iar_test = iar_test + 1
            iar_pass = iar_pass + 1
            print *, "  PASS   ", topic

        end subroutine int_array_compare

        ! Compare real DP 3D array
        subroutine real3d_array_compare(expected, result, dims, topic)
            implicit none

            real(kind=8), pointer, intent(in) :: expected(:,:,:)
            real(kind=8), pointer, intent(in) :: result(:,:,:)
            integer, pointer, intent(in) :: dims(:)
            character(len=*), intent(in) :: topic

            integer :: x_iter
            integer :: y_iter
            integer :: z_iter

            d3_test = d3_test + 1

            do x_iter = 1, dims(1), 1
                do y_iter = 1, dims(1), 1
                    do z_iter = 1, dims(1), 1
                        if (expected(x_iter, y_iter, z_iter) /= result(x_iter, y_iter, z_iter)) then
                            d3_fail = d3_fail + 1
                            print *, "**FAIL** ", topic, " - Expected: ", expected(x_iter, y_iter, z_iter), &
                                    " Result: ", result(x_iter, y_iter, z_iter), " Index: ", x_iter, y_iter, z_iter
                            return
                        end if
                    end do
                end do
            end do

            d3_pass = d3_pass + 1
            print *, "  PASS   ", topic

        end subroutine real3d_array_compare

        ! Compare SP real arrays
        subroutine reals_array_compare(expected, result, topic)
            implicit none

            real(kind=4), intent(in) :: expected(:)
            real(kind=4), intent(in) :: result(:)
            character(len=*), intent(in) :: topic
            integer :: l_ex
            integer :: l_rs
            integer :: iter

            l_ex = size(expected)
            l_rs = size(result)

            if(l_ex /= l_rs) then
                tests = tests + 1
                fail = fail + 1
                rsa_test = rsa_test + 1
                rsa_fail = rsa_fail + 1
                print *, "**FAIL** ", topic, " - mismatched length - Expected: ", l_ex, " Result: ", l_rs
                print *, "Expected: ", expected
                print *, "Result:   ", result
                return
            else
                do iter = 1, l_rs, 1
                    if(expected(iter) /= result(iter)) then
                        tests = tests + 1
                        fail = fail + 1
                        rsa_test = rsa_test + 1
                        rsa_fail = rsa_fail + 1
                        print *, "**FAIL** ", topic, " - Expected: ", expected(iter), " Result: ", result(iter),&
                                " Index: ", iter
                        return
                    end if
                end do
            end if

            tests = tests + 1
            pass = pass + 1
            rsa_test = rsa_test + 1
            rsa_pass = rsa_pass + 1
            print *, "  PASS   ", topic

        end subroutine reals_array_compare

        ! Compare DP real arrays
        subroutine reald_array_compare(expected, result, topic)
            implicit none

            real(kind=8), intent(in) :: expected(:)
            real(kind=8), intent(in) :: result(:)
            character(len=*), intent(in) :: topic
            integer :: l_ex
            integer :: l_rs
            integer :: iter

            l_ex = size(expected)
            l_rs = size(result)

            if(l_ex /= l_rs) then
                tests = tests + 1
                fail = fail + 1
                rda_test = rda_test + 1
                rda_fail = rda_fail + 1
                print *, "**FAIL** ", topic, " - mismatched length - Expected: ", l_ex, " Result: ", l_rs
                print *, "Expected: ", expected
                print *, "Result:   ", result
                return
            else
                do iter = 1, l_rs, 1
                    if(expected(iter) /= result(iter)) then
                        tests = tests + 1
                        fail = fail + 1
                        rda_test = rda_test + 1
                        rda_fail = rda_fail + 1
                        print *, "**FAIL** ", topic, " - Expected: ", expected(iter), " Result: ", result(iter),&
                                " Index: ", iter
                    end if
                end do
            end if

            tests = tests + 1
            pass = pass + 1
            rda_test = rda_test + 1
            rda_pass = rda_pass + 1
            print *, "  PASS   ", topic

        end subroutine reald_array_compare

        ! Compare strings
        subroutine string_ptr_compare(expected, result, topic)
            implicit none

            character(len=*), intent(in) :: expected
            character(len=:), pointer, intent(in) :: result
            character(len=*), intent(in) :: topic
            character :: temp_ex
            character :: temp_re
            integer :: iter = 0
            integer :: l_ex
            integer :: l_rs

            l_ex = len(expected)
            l_rs = len(result)

            if(l_ex /= l_rs) then
                tests = tests + 1
                fail = fail + 1
                str_test = str_test + 1
                str_fail = str_fail + 1
                print *, "**FAIL** ", topic, " - mismatched length - Expected: ", l_ex, " Result: ", l_rs
                print *, "Expected: ", expected
                print *, "Result:   ", result
                return
            else
                do iter = 1, l_rs, 1
                    temp_ex = expected(iter:iter)
                    temp_re = result(iter:iter)
                    if(temp_ex /= temp_re) then
                        tests = tests + 1
                        fail = fail + 1
                        str_test = str_test + 1
                        str_fail = str_fail + 1
                        print *, "**FAIL** ", topic, " - Expected: ", temp_ex, " Result: ", temp_re, " Index: ", iter
                        print *, "Expected: ", expected
                        print *, "Result:   ", result
                        return
                    end if
                end do
            end if

            tests = tests + 1
            pass = pass + 1
            str_test = str_test + 1
            str_pass = str_pass + 1
            print *, "  PASS   ", topic, result
        end subroutine string_ptr_compare

        ! Reset test Suite. Print summary
        subroutine complete_test()

            print *, "========================================="
            print *, "| Total tests:   ", tests
            print *, "| Total Pass:    ", pass
            print *, "| Total Fail:    ", fail
            print *, "| Int Tests:     ", int_test
            print *, "| Int Passes:    ", int_pass
            print *, "| Int Fails:     ", int_fail
            print *, "| Real SP Tests: ", rls_test
            print *, "| Real SP Passes:", rls_pass
            print *, "| Real SP Fails: ", rls_fail
            print *, "| Real DP Tests: ", rld_test
            print *, "| Real DP Passes:", rld_pass
            print *, "| Real DP Fails: ", rld_fail
            print *, "| Int arr Tests: ", iar_test
            print *, "| Int arr Passes:", iar_pass
            print *, "| Int arr Fails: ", iar_fail
            print *, "| 3D arr Tests: ", d3_test
            print *, "| 3D arr Passes:", d3_pass
            print *, "| 3D arr Fails: ", d3_fail
            print *, "| R SP Arr Tests:", rsa_test
            print *, "| R SP Arr Pass: ", rsa_pass
            print *, "| R SP Arr Fails:", rsa_fail
            print *, "| R DP Arr Tests:", rda_test
            print *, "| R DP Arr Pass: ", rda_pass
            print *, "| R DP Arr Fails:", rda_fail
            print *, "| Str Tests:     ", str_test
            print *, "| Str Passes:    ", str_pass
            print *, "| Str Fail:      ", str_fail
            print *, "========================================="

            ! Reset all counters
            tests = 0
            pass = 0
            fail = 0
            int_test = 0
            int_pass = 0
            int_fail = 0
            rls_test = 0
            rls_pass = 0
            rls_fail = 0
            rld_test = 0
            rld_pass = 0
            rld_fail = 0
            iar_test = 0
            iar_pass = 0
            iar_fail = 0
            d3_test = 0
            d3_pass = 0
            d3_fail = 0
            str_test = 0
            str_pass = 0
            str_fail = 0

        end subroutine complete_test
end module unit_test
