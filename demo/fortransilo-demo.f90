program demo_fs
        use fortransilo
        implicit none
        include "silo_f9x.inc"

        integer :: file
        integer :: err, ierr

        ! DATA WRITTEN OUT
        character(len=13) :: d3_qmesh_desc = "Demo Quadmesh"
        character(len=4) :: d3_qmesh_name = "demo"
        integer :: out_d3_qmesh_ndims = 3
        integer, dimension(3) :: out_d3_qmesh_dims = (/2, 3, 4/)
        real(kind=8), dimension(2) :: out_d3_qmesh_x = (/1.123456789, 2.987654321/)
        real(kind=8), dimension(3) :: out_d3_qmesh_y = (/2.192837465, 4.129834765, 6.123987456/)
        real(kind=8), dimension(4) :: out_d3_qmesh_z = (/1.000000001, 2.0000000002, 9.0000000005, 18.00000000009/)
        character(len=6) :: out_d3_qmesh_xname = "X Axis"
        character(len=6) :: out_d3_qmesh_yname = "Y Axis"
        character(len=6) :: out_d3_qmesh_zname = "Z Axis"
        integer :: out_d3_qmesh_optlist

        ! DATA READ IN
        character(len=:), pointer :: in_d3_qmesh_xname
        character(len=:), pointer :: in_d3_qmesh_yname
        character(len=:), pointer :: in_d3_qmesh_zname
        real(kind=8), pointer :: in_d3_qmesh_x(:) => null()
        real(kind=8), pointer :: in_d3_qmesh_y(:) => null()
        real(kind=8), pointer :: in_d3_qmesh_z(:) => null()
        integer, pointer :: in_d3_qmesh_dims(:) => null()
        integer :: in_d3_qmesh_ndims
        integer :: in_d3_qmesh_datatype
        integer :: in_d3_qmesh_coordtype
        integer :: in_d3_qmesh_optlist
        integer :: cycle_in

        print *, "Started fortran silo demo"

        ! Create demo file
        print *, "Creating Demo file"
        err = dbcreate("demo.silo",9,DB_CLOBBER,DB_LOCAL,"Demo file",9,DB_HDF5,file)
        call check_error(err, "create file")

        ! Create Optlist
        print *, "Writing optlist"
        err = dbmkoptlist(1, out_d3_qmesh_optlist)
        call check_error(err, "generate optlist")
        err = dbaddiopt(out_d3_qmesh_optlist, DBOPT_CYCLE, 4)
        call check_error(err, "set cycle")

        ! Put Mesh
        print *, "Writing mesh"
        err = dbputqm(file, d3_qmesh_name, len(d3_qmesh_name), out_d3_qmesh_xname, 6, &
                & out_d3_qmesh_yname, 6, out_d3_qmesh_zname, 6, out_d3_qmesh_x, out_d3_qmesh_y, out_d3_qmesh_z, &
                & out_d3_qmesh_dims, out_d3_qmesh_ndims, DB_DOUBLE, DB_COLLINEAR, out_d3_qmesh_optlist, ierr)
        call check_error(err)

        ! Closing file
        err = dbclose(file)
        call check_error(err, "close file")

        ! Re-open file
        err = dbopen("demo.silo", 9, DB_HDF5, DB_APPEND, file)
        call check_error(err, "open file")

        ! Read quadmesh
        call dbgetqm(file,d3_qmesh_name, in_d3_qmesh_xname, in_d3_qmesh_yname, in_d3_qmesh_zname, &
                &  in_d3_qmesh_x, in_d3_qmesh_y, in_d3_qmesh_z, in_d3_qmesh_dims, &
                & in_d3_qmesh_ndims, in_d3_qmesh_datatype, in_d3_qmesh_coordtype, in_d3_qmesh_optlist)

        ! Read cycle
        err = dbgetopt(in_d3_qmesh_optlist, DBOPT_CYCLE, cycle_in)
        call check_error(err)
        
        print *, "Data in:"
        print *, in_d3_qmesh_x
        print *, in_d3_qmesh_y
        print *, in_d3_qmesh_z

end program demo_fs
