program write_mmesh
        implicit none
        include "silo_f9x.inc"

        ! STATUS VARIABLE DEFS
        integer :: err
        integer :: ierr

        ! FILE VARIABLE DEFS
        character(len=12) :: filename = "example.silo"
        character(len=4) :: file_desc = "test"
        integer :: example_file

        ! MESH VARIABLE DEFS
        character(len=17) :: mmesh_name = "example_multimesh"
        integer, parameter :: nmesh = 2
        character(len=5), dimension(:), allocatable :: meshnames
        integer, dimension(nmesh) :: meshname_lens = (/5, 5/)
        integer, dimension(nmesh) :: meshtypes = (/130, 130/)        ! These should all be 130
        integer :: optlist

        ! OPEN FILE
        print *, "Creating file"
        err = dbcreate(filename,12,DB_CLOBBER,DB_LOCAL,file_desc,4,DB_HDF5,example_file)
        call err_check(err)

        ! CREATE OPTLIST
        print *, "Creating optlist"
        err = dbmkoptlist(1, optlist)
        call err_check(err)

        ! POPULATE MESHNAMES
        print *, "Populating meshnames"
        allocate(meshnames(nmesh))
        write(meshnames(1), '(a)') "mesh1"
        write(meshnames(2), '(a)') "mesh2"
        !meshmames(3) = "mesh3"

        ! SET STRLEN
        err = dbset2dstrlen(len(meshnames))
        call err_check(err)

        print *, "Meshnames: ", meshnames

        ! CREATE MULTIMESH
        print *, "Writing multimesh"
        err = dbputmmesh(example_file, mmesh_name, len(mmesh_name), nmesh, meshnames, meshname_lens, meshtypes, DB_F77NULL, ierr)
        call err_check(err)

        ! CLOSING FILE
        print *, "Closing file"
        err = dbclose(example_file)
        call err_check(err)

        contains
                subroutine err_check(err)
                        implicit none

                        integer,intent(in) :: err

                        print *, "Error code: ", err
                end subroutine err_check
end program write_mmesh
