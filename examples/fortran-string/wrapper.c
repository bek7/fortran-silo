#include<stdio.h>
#include<stdlib.h>
#include<string.h>

void write_int_(int* in){
	printf("%i, %i, %i\n", in[0], in[1], in[2]);
}

char* f_to_c_str(char* f_str, int* len) {
    char* c_str = malloc(*len+1);
    strncpy(c_str, f_str, *len);

    return c_str;
}

void cleanup_c_str(char* c_str) {
    free(c_str);
}

void write_string_(char* in, int* len){
    char* c_str = f_to_c_str(in, len);
	printf("Fortran string in C: %lu %d %s\n", strlen(in), *len, in);
	printf("C string in C: %lu %d %s\n", strlen(c_str), *len, c_str);
    cleanup_c_str(c_str);
}
