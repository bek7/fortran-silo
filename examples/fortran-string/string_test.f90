PROGRAM string_test
       use iso_c_binding
       implicit none
       character(LEN=10), target :: name        ! Do we want character(C_CHAR) ?
       character(LEN=10), pointer :: name_ptr => null()
                        ! name_ptr(:) does not compile
       type(c_ptr) :: name_c_ptr

      name = "00001/Mesh"

       name_ptr => name ! Either unequal character length or rank compile error.

       name_c_ptr = C_LOC(name_ptr)

       print *, "Name init: ", name     ! These two should be the same
       print *, "Name ptr: ", name_ptr

       call write_string(name, len(name))
END PROGRAM string_test
