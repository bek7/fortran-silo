#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include <math.h>

const char* STR1 = "Hello world!";
const char* STR2 = "Test1";
const char* STR3 = "t";


void populate_arr_(char*** farr){
	int i;
	printf("Populating array in C...\n");

	char** arr = malloc(sizeof(char*) * 3);

	arr[0] = malloc(sizeof(char) * strlen(STR1));
	arr[1] = malloc(sizeof(char) * strlen(STR2));
	arr[2] = malloc(sizeof(char) * strlen(STR3));

	strcpy(arr[0], STR1);
	strcpy(arr[1], STR2);
	strcpy(arr[2], STR3);

	for(i = 0; i < 3; ++i){
        printf("%d %s\n", i, arr[i]);
	}

    *farr = arr;

	printf("Exiting C\n");
}

int* strlen_(char* str){
    // BUG::: This fucntion has a bug it returns a pointer to a local variable.
	int* result;
	*result = strlen(str);
	return result;
}

void get_str_at_index_(char** s, char*** strs, int* fortran_index){
    // fortran will provide an index, but it is one indexed.
    // c will see the same structure as 0 indexed.
    // We subtract 1 internally to resolve the 0/1 indexing issue.
    *s = strs[0][(*fortran_index) - 1];
}

void get_longest_str_(int* len, int* items, char*** arr){
	int longest = 0;
	int i;
	printf("Determining longest string\n");
	for(i = 0; i < *items; ++i){
        longest = fmax(longest, strlen(arr[0][i]));
	}
	*len = longest;
}
