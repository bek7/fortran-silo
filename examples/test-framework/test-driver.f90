program utest_example
    use unit_test
    implicit none

    !!!!!!!!!!!!!!!!!!!!!!
    ! VARIABLE DECLARATION
    !!!!!!!!!!!!!!!!!!!!!!
    integer :: int1, int2, int3, int4, int5, int6, int7, int8
    integer :: irs1, irs2, irs3, irs4, irs5, irs6, irs7, irs8

    real(kind=4) :: rls1, rls2, rls3, rls4
    real(kind=4) :: rrs1, rrs2, rrs3, rrs4

    real(kind=8) :: rld1, rld2, rld3, rld4
    real(kind=8) :: rrd1, rrd2, rrd3, rrd4

    !!!!!!!!!!!!!!!!!!!!!!
    ! SET UP TESTS
    !!!!!!!!!!!!!!!!!!!!!!

    ! Integer test 1
    ! PASS
    int1 = 1
    irs1 = 1

    ! Integer test 2
    ! PASS
    int2 = 2
    irs2 = 2

    ! Integer test 3
    ! FAIL
    int3 = 3
    irs3 = 4

    ! Integer test 4
    ! PASS
    int4 = 5
    irs4 = 5

    ! Integer test 5
    ! FAIL
    int5 = 8
    irs5 = 17

    ! Integer test 6
    ! PASS
    int6 = 13
    irs6 = 13

    ! Integer test 7
    ! PASS
    int7 = 21
    irs7 = 21

    ! Integer test 8
    ! PASS
    int8 = 34
    irs8 = 34

    ! Real SP test 1
    ! PASS
    rls1 = 1.234567
    rrs1 = 1.234567

    ! Real SP test 2
    ! FAIL
    rls2 = 10.9876
    rrs2 = 10.9856

    ! Real SP test 3
    ! PASS
    rls3 = 1024.768
    rrs3 = 1024.768

    ! Real SP test 4
    ! PASS
    rls4 = 123.456
    rrs4 = 123.456

    ! Real DP test 1
    ! FAIL
    rld1 = 0.00000000001
    rrd1 = 0.00000000002

    ! Real DP test 2
    ! PASS
    rld2 = 10.987654321
    rrd2 = 10.987654321

    ! Real DP test 3
    ! PASS
    rld3 = 0.0543
    rrd3 = 0.0543

    ! Real DP test 4
    ! PASS
    rld4 = 0.7
    rrd4 = 0.7

    !!!!!!!!!!!!!!!!!!!!!!
    ! RUN TESTS
    !!!!!!!!!!!!!!!!!!!!!!

    ! Int tests
    call int_compare(int1, irs1, "Int 1")
    call int_compare(int2, irs2, "Int 2")
    call int_compare(int3, irs3, "Int 3")
    call int_compare(int4, irs4, "Int 4")
    call int_compare(int5, irs5, "Int 5")
    call int_compare(int6, irs6, "Int 6")
    call int_compare(int7, irs7, "Int 7")
    call int_compare(int8, irs8, "Int 8")
    call reals_compare(rls1, rrs1, "Real 1S")
    call reals_compare(rls2, rrs2, "Real 2S")
    call reals_compare(rls3, rrs3, "Real 3S")
    call reals_compare(rls4, rls4, "Real 4S")
    call reald_compare(rld1, rrd1, "Real 1D")
    call reald_compare(rld2, rrd2, "Real 2D")
    call reald_compare(rld3, rrd3, "Real 3D")
    call reald_Compare(rld4, rrd4, "Real 4D")

    call complete_test

end program utest_example