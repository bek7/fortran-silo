# Install fortran mod files

file(COPY fortransilo_stringhandler.mod DESTINATION ${CMAKE_INSTALL_PREFIX}/include)
file(COPY optlist.mod DESTINATION ${CMAKE_INSTALL_PREFIX}/include)
file(COPY fortransilo_private.mod DESTINATION ${CMAKE_INSTALL_PREFIX}/include)
file(COPY fortransilo.mod DESTINATION ${CMAKE_INSTALL_PREFIX}/include)
