#
# Find the native SZIP includes and library
#
# SZIP_INCLUDE_DIR - where to find H5public.h, etc.
# SZIP_LIBRARIES   - List of fully qualified libraries to link against when using hdf5.
# SZIP_FOUND       - Do not attempt to use hdf5 if "no" or undefined.
message("Path = ${SZIP_DIR}")

# First try to find using the user-specific HDF info.
find_path(SZIP_INCLUDE_DIR 
          NAMES szlib.h
          PATHS ${SZIP_DIR}/include 
                ${SZIP_INC}
          NO_DEFAULT_PATH
)
find_library(SZIP_LIBRARY_CORE sz dzdll
             PATHS ${SZIP_DIR}/lib
                   ${SZIP_LIB}
             NO_DEFAULT_PATH
)

# if that fails, use the full path
find_path(SZIP_INCLUDE_DIR 
          NAMES szlib.h
          PATHS ${SZIP_DIR}/include 
                ${SZIP_INC}
)
find_library(SZIP_LIBRARY sz szdll
             PATHS ${SZIP_DIR}/lib
                   ${SZIP_LIB}
)
message("SZIP_INCLUDE_DIR: ${SZIP_INCLUDE_DIR}")
message("SZIP_LIBRARY:     ${SZIP_LIBRARY}")

set( SZIP_FOUND "NO" )
if(SZIP_INCLUDE_DIR)
  if(SZIP_LIBRARY)
    set( SZIP_LIBRARIES ${SZIP_LIBRARY})
    set( SZIP_FOUND "YES" )
  endif(SZIP_LIBRARY)
elseif(SZIP_INCLUDE_DIR)
  message("SZIP not found. Try setting SZIP_DIR.")
endif(SZIP_INCLUDE_DIR)


