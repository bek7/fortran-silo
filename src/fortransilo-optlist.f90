module optlist
    implicit none

    ! Will be called by the user
    interface dbgetopt
        module procedure get_opt_i
        module procedure get_opt_i_arr
        module procedure get_opt_r
!        module procedure get_opt_r_arr
        module procedure get_opt_d
        module procedure get_opt_d_arr
        module procedure get_opt_char
    end interface dbgetopt

    contains

        ! Retrieve integer option from optlist
        ! Arguments:
        !   id:         Identifier for option list to be used
        !   opt:        Identifier for option to be retrieved (Uses SILO constants)
        !   val:        Value to be set
        integer function get_opt_i(id, opt, val) result(err)
            use iso_c_binding
            use fortransilo_private
            implicit none
            include "silo_f9x.inc"

            integer, intent(in) :: id
            integer, intent(in) :: opt
            integer, intent(out) :: val

            integer(C_INT) :: val_c

            err = 0

            call get_opt_i_c(id, opt, val_c)

            val = int(val_c)

        end function get_opt_i

        ! Retrieve integer array option from optlist
        ! Arguments:
        !   id:         Identifier for option list to be used
        !   opt:        Identifier for option to be retrieved (Uses SILO constants)
        !   val:        Value to be set
        integer function get_opt_i_arr(id, opt, arr) result(err)
            use iso_c_binding
            use fortransilo_private
            implicit none
            include "silo_f9x.inc"

            integer, intent(in) :: id
            integer, intent(in) :: opt
            integer, pointer, intent(out) :: arr(:)

            type(C_PTR) :: ptr

            err = 0

            call get_opt_i_arr_c(id, opt, ptr)

            call c_f_pointer(ptr, arr, shape=[3])

            err = 0

        end function get_opt_i_arr

        ! Retrieve real SP option from optlist
        ! Arguments:
        !   id:         Identifier for option list to be used
        !   opt:        Identifier for option to be retrieved (Uses SILO constants)
        !   val:        Value to be set
        integer function get_opt_r(id, opt, val) result(err)
            use iso_c_binding
            use fortransilo_private
            implicit none
            include "silo_f9x.inc"

            integer, intent(in) :: id
            integer, intent(in) :: opt
            real(kind=4), intent(out) :: val

            real(C_FLOAT) :: val_c

            err = 0

            call get_opt_r_c(id, opt, val_c)

            val = real(val_c, 4)

        end function get_opt_r

        ! Retrieve real DP option from optlist
        ! Arguments:
        !   id:         Identifier for option list to be used
        !   opt:        Identifier for option to be retrieved (Uses SILO constants)
        !   val:        Value to be set
        integer function get_opt_d(id, opt, val) result(err)
            use iso_c_binding
            use fortransilo_private
            implicit none
            include "silo_f9x.inc"

            integer, intent(in) :: id
            integer, intent(in) :: opt
            real(kind=8), intent(out) :: val

            real(C_DOUBLE) :: val_c

            err = 0

            call get_opt_d_c(id, opt, val_c)

            val = real(val_c, 8)

        end function get_opt_d

        ! Retrieve real DP array option from optlist
        ! Arguments:
        !   id:         Identifier for option list to be used
        !   opt:        Identifier for option to be retrieved (Uses SILO constants)
        !   val:        Values to be set
        integer function get_opt_d_arr(id, opt, arr) result(err)
            use iso_c_binding
            use fortransilo_private
            implicit none
            include "silo_f9x.inc"

            integer, intent(in) :: id
            integer, intent(in) :: opt
            real(kind=8), pointer, intent(out) :: arr(:)

            type(C_PTR) :: ptr

            err = 0

            call get_opt_d_arr_c(id, opt, ptr)

            call c_f_pointer(ptr, arr, shape=[size(arr)])

            err = 0

        end function get_opt_d_arr

        
        integer function get_opt_char(id, opt, val) result(err)
            use iso_c_binding
            use fortransilo_private
            implicit none
            include "silo_f9x.inc"

            integer, intent(in) :: id
            integer, intent(in) :: opt
            character(len=:), pointer, intent(out) :: val

            type(C_PTR) :: str_ptr

            err = 0

            call get_opt_str_c(id, opt, str_ptr)

            allocate(character(len=strlen(str_ptr)) :: val)
            call c_f_pointer(str_ptr, val)

        end function get_opt_char

end module optlist
