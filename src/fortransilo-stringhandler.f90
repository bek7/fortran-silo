! Module full of useful things to work with strings between C and
! Fortran
module fortransilo_stringhandler
        use iso_c_binding
        implicit none

        interface
                subroutine populate_arr(f_array) bind(c)
                        use iso_c_binding
                        type(C_PTR) :: f_array
                end subroutine
        end interface

        interface
                subroutine get_str_at_index(string_out, array, idx) bind(c)
                        use iso_c_binding
                        type(C_PTR) :: string_out
                        type(C_PTR) :: array
                        integer(C_INT) :: idx
                end subroutine
        end interface

        interface
                subroutine get_longest_str(longest, items, array) bind(c)
                        use iso_c_binding
                        integer(C_INT) :: longest
                        integer(C_INT) :: items
                        type(C_PTR) :: array
                end subroutine
        end interface

        contains
               integer function populate_arr_2d(arr, n_strings, c_arr_ptr) result(err)
                       use fortransilo_private
                       use iso_c_binding
                       implicit none

                       character(len=:), pointer, intent(inout) :: arr(:)
                       integer, intent(in) :: n_strings
                       type(C_PTR) :: c_arr_ptr

                       character(len=:), pointer :: char_temp

                       integer :: iter

                       type(C_PTR) :: cur_str_iter
                       integer :: longest

                       call get_longest_str(longest, n_strings, c_arr_ptr)

                       call alloc_arr(arr, n_strings, longest)

                        do iter = 1, n_strings
                                call get_str_at_index(cur_str_iter, c_arr_ptr, iter)
                                allocate(character(len=strlen(cur_str_iter)) :: char_temp)
                                call c_f_pointer(cur_str_iter, char_temp)
                                call copy_str(arr, char_temp, iter)
!                                deallocate(char_temp)
                        end do

                err = 0

                end function populate_arr_2d

                subroutine alloc_arr(arr, n_str, strlen)
                        implicit none

                        character(len=:), pointer, intent(out) :: arr(:)
                        integer, intent(in) :: n_str
                        integer, intent(in) :: strlen

                        allocate(character(len=strlen) :: arr(n_str))
                end subroutine alloc_arr

                subroutine copy_str(arr, str_tmp, idx)
                        implicit none

                        character(len=:), pointer, intent(inout) :: arr(:)
                        character(len=:), pointer, intent(in) :: str_tmp
                        integer, intent(in) :: idx

                        integer :: iter
                        integer :: length

                        ! Copy string and add null chars to end of array
                        arr(idx)=trim(adjustl(str_tmp))//char(0)

                end subroutine copy_str
end module fortransilo_stringhandler
