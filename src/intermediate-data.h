//
// Created by Brendan Kristiansen on 3/7/18.
//

#ifndef FORTRAN_SILO_INTERMEDIATE_DATA_H
#define FORTRAN_SILO_INTERMEDIATE_DATA_H

typedef struct DBquadmesh_intermediate {
    int            id;
    int            block_no;
    int            group_no;
    char*          name;
    int            cycle;
    int            coord_sys;
    int            major_order;
    int*           stride;
    int            coordtype;
    int            facetype;
    int            planar;
    float*         x_coords_sp;
    float*         y_coords_sp;
    float*         z_coords_sp;
    double*        x_coords_dp;
    double*        y_coords_dp;
    double*        z_coords_dp;
    int            datatype;
    float          time;
    double         dtime;

    /*
     * The following two fields really only contain 3 elements.  However, silo
     * contains a bug in PJ_ReadVariable() as called by DBGetQuadmesh() which
     * can cause three doubles to be stored there instead of three floats.
     */
    float*          min_extents;
    float*          max_extents;
    char*          xname;
    char*          yname;
    char*          zname;
//    char          *units[3];
    int            ndims;
    int            nspace;
    int            nnodes;
    int*           dims;
    int            origin;
    int*           min_index;
    int*           max_index;
    int*           base_index;
    int*           start_index;
    int*           size_index;
    int            guihide;
//    char          *mrgtree_name;
//    char          *ghost_node_labels;
//    char          *ghost_zone_labels;
//    char         **alt_nodenum_vars;
//    char         **alt_zonenum_vars;
    int             coords_size;
}DBquadmesh_intermediate;

typedef struct DBquadvar_intermediate {
    int            id;
    char*          name;
    char*          units;
    char*          label;
    int            cycle;
    int            meshid;
    void**         vals;
    int            datatype;
    int            nels;
    int            nvals;
    int            ndims;
    int*           dims;
    int            major_order;
    int*           stride;
    int*           min_index;
    int*           max_index;
    int            origin;
    float          time;
    double         dtime;

//    /*
//     * The following field really only contains 3 elements.  However, silo
//     * contains a bug in PJ_ReadVariable() as called by DBGetQuadvar() which
//     * can cause three doubles to be stored there instead of three floats.
//     */
    double*        align;
//    void**       mixvals;
    int            mixlen;
    int            use_specmf;
    int            ascii_labels;
    char*          meshname;
    int            guihide;
//    char**       region_pnames;
    int            conserved;
    int            extensive;
    int            centering;
    double         missing_value;
} DBquadvar_intermediate;

typedef struct DBmultimesh_intermediate {
/*----------- Multi-Block Mesh -----------*/
    int            id;
    int            nblocks;
    int            ngroups;
    int*           meshids;
    char**        meshnames;
    int*          meshtypes;
    int*          dirids;
    int            blockorigin;
    int            grouporigin;
    int            extentssize;
    double*       extents;
    int*          zonecounts;
    int*          has_external_zones;
    int            guihide;
    int            lgroupings;
    int*          groupings;
//    char**        groupnames;
//    char*         mrgtree_name;
    int            tv_connectivity;
    int            disjoint_mode;
    int            topo_dim;
//    char*         file_ns;
//    char*         block_ns;
    int            block_type;
    int*          empty_list;
    int            empty_cnt;
    int            repr_block_idx;
//    char**        alt_nodenum_vars;
//    char**        alt_zonenum_vars;
//    char*         meshnames_alloc;
} DBmultimesh_intermediate;

typedef struct DBmultivar_intermediate {
/*----------- Multi-Block Variable -----------*/
    int            id;
    int            nvars;
    int            ngroups;
    char         **varnames;
    int*           vartypes;
    int            blockorigin;
    int            grouporigin;
    int            extentssize;
    double*        extents;
    int            guihide;
//    char**         region_pnames;
    char*          mmesh_name;
    int            tensor_rank;
    int            conserved;
    int            extensive;
    char*          file_ns;
    char*          block_ns;
    int            block_type;
    int*           empty_list;
    int            empty_cnt;
    int            repr_block_idx;
    double         missing_value;
    char*          varnames_alloc;
} DBmultivar_intermediate;

#endif //FORTRAN_SILO_INTERMEDIATE_DATA_H
