! Created by Brendan Kristiansen on 4/6/18.

module dbstring

    type db_string
        character, pointer  :: string(:) => null()
    end type db_string

    contains

        ! Parse a pointer to a C string to a DB_string
        subroutine parse_c_string(dbstring, string_in)
            use iso_c_binding
            use fortransilo_private
            implicit none

            type(db_string) :: dbstring
            type(C_PTR)     :: string_in

            call c_f_pointer(string_in, dbstring%string, shape=[strlen(string_in)])

        end subroutine parse_c_string

end module dbstring