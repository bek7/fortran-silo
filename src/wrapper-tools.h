//
// Created by Brendan Kristiansen on 2/13/18.
//

#ifndef FORTRAN_SILO_TEST_INFO_H
#define FORTRAN_SILO_TEST_INFO_H

const int MAX_OPTS = 14;

/*
 * Prints the target and physical address of a pointer
 */
void print_addresses(void* ptr, char* object_name){
    printf("%s pointer:\t %p\n", object_name, &ptr);
    printf("%s object:\t %p\n", object_name, ptr);
}

/*
 * Checks the target address and physical address of 2 pointers
 */
void check_pointer_consistency(void* ptr1, void* ptr2, char* obj1, char* obj2){
    printf("%s and %s ", obj1, obj2);
    if(ptr1 == ptr2){
        printf("point to same address (%p)\n", ptr1);
    }else{
        printf("point to different addresses (%p and %p)\n", ptr1, ptr2);
    }
    if(&ptr1 == &ptr2){
        printf("%s and %s share same location (%p)\n", obj1, obj2, &ptr1);
    }
}

/*
 * Prints a big divider to make testing easier to read
 */
void big_divider(char* label){
    printf("\n-------------------------------------------\n\n");
    printf("%s\n", label);
    printf("-------------------------------------------\n\n");

}

/*
 * Copies a string if it exists, writes null terminator to new string
 * if not.
 */
void copy_string(char* original_string, char* new_string) {
    if(original_string == NULL){
        new_string = "\0";
        return;
    }else{
        strcpy(new_string, original_string);
    }
}

char* allocate_string(char* original_string){
    char* string_out;
    if(original_string == NULL){
        string_out = (char*) malloc(sizeof(char));
        string_out = "\0";
    }else{
        string_out = (char*) malloc(sizeof(char) * strlen(original_string));
    }
    return  string_out;
}

#endif //FORTRAN_SILO_TEST_INFO_H