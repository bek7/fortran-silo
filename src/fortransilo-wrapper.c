//
// Created by Brendan Kristiansen on 1/25/18.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <silo.h>
#include "wrapper-tools.h"
#include "intermediate-data.h"

const char* QUADVARNAME = "00001/fun";
const char* MULTIMESHNAME = "Mesh";
const char* MULTIVARNAME = "fun";

int total_size(int, int*);
char* f_to_c_str(char*, int*);

void init_test_(){
    DBSetDeprecateWarnings(0);
}

/*
 * Read varialbe. Called from Fortran
 */
void load_db_getvar_c(int * dbid, char* name, int* lname, double* var){
  
    char* name_loc = f_to_c_str(name, lname);

    DBfile* file = (DBfile *) DBFortranAccessPointer(*dbid);
    if(file == NULL){
        return;
    }
    *var = *(double*) DBGetVar(file, name_loc);
    return;
}

/*
 * Read Quadmesh. Called from Fortran
 */
void load_db_quadmesh_c(int* dbid, char* name, int* lname, DBquadmesh_intermediate* quadmesh_out, int* optlist_id){
    int i, j, array_size;
    int coords_iter = 0;
    float* float_arr;

    char* name_loc = f_to_c_str(name, lname);

    DBfile* file = (DBfile *) DBFortranAccessPointer(*dbid);
    if(file == NULL){
        return;
    }
    DBquadmesh* mesh = DBGetQuadmesh(file, name_loc);
    if(mesh == NULL){
        return;
    }

    DBquadmesh_intermediate* mesh_i = (DBquadmesh_intermediate*)malloc(sizeof(DBquadmesh_intermediate));

    /*
     * Allocate Strings
     */
    mesh_i->name = (char*) malloc(sizeof(char) * (int) strlen(mesh->name));
    mesh_i->xname = allocate_string(mesh->labels[0]);
    mesh_i->yname = allocate_string(mesh->labels[1]);
    mesh_i->zname = allocate_string(mesh->labels[2]);

    /*
     * Convert DBquadmesh to intermediate struct
     */
    mesh_i->id = mesh->id;
    mesh_i->block_no = mesh->block_no;
    mesh_i->group_no = mesh->group_no;
    copy_string(mesh->name, mesh_i->name);
    mesh_i->cycle = mesh->cycle;
    mesh_i->coord_sys = mesh->coord_sys;
    mesh_i->major_order = mesh->major_order;
    mesh_i->stride = mesh->stride;
    mesh_i->coordtype = mesh->coordtype;
    mesh_i->facetype = mesh->facetype;
    mesh_i->planar = mesh->planar;
    // Coords conversion done below
    mesh_i->datatype = mesh->datatype;
    mesh_i->time = mesh->time;
    mesh_i->dtime = mesh->dtime;
    mesh_i->min_extents = mesh->min_extents;
    mesh_i->max_extents = mesh->min_extents;
    copy_string(mesh->labels[0], mesh_i->xname);
    copy_string(mesh->labels[1], mesh_i->yname);
    copy_string(mesh->labels[2], mesh_i->zname);
    // TODO: Units
    mesh_i->ndims = mesh->ndims;
    mesh_i->nspace = mesh->nspace;
    mesh_i->nnodes = mesh->nnodes;
    mesh_i->dims = mesh->dims;
    mesh_i->origin = mesh->origin;
    mesh_i->min_index = mesh->min_index;
    mesh_i->max_index = mesh->max_index;
    mesh_i->base_index = mesh->base_index;
    mesh_i->start_index = mesh->start_index;
    mesh_i->size_index = mesh->size_index;
    mesh_i->guihide = mesh->guihide;

    /*
     * Coords conversion
     */
    array_size = total_size(mesh_i->ndims, mesh_i->dims);
    switch(mesh_i->datatype){
        case DB_FLOAT:
            mesh_i->x_coords_sp = (float*)((mesh->coords)[0]);
            mesh_i->y_coords_sp = (float*)((mesh->coords)[1]);
            mesh_i->z_coords_sp = (float*)((mesh->coords)[2]);
            break;
        case DB_DOUBLE:
            mesh_i->x_coords_dp = (double*)((mesh->coords)[0]);
            mesh_i->y_coords_dp = (double*)((mesh->coords)[1]);
            mesh_i->z_coords_dp = (double*)((mesh->coords)[2]);
            break;
        default:
            return;
    }
    mesh_i->coords_size = array_size;

    /*
     * Build optlist
     */
    if (*optlist_id != -99) {
        DBoptlist *c_optlist = DBMakeOptlist(MAX_OPTS);

        DBAddOption(c_optlist, DBOPT_GROUPNUM, (void *) &(mesh_i->group_no));
        DBAddOption(c_optlist, DBOPT_CYCLE, (void *) &(mesh_i->cycle));
        DBAddOption(c_optlist, DBOPT_COORDSYS, (void *) &(mesh_i->coord_sys));
        DBAddOption(c_optlist, DBOPT_MAJORORDER, (void *) &(mesh_i->major_order));
        DBAddOption(c_optlist, DBOPT_FACETYPE, (void *) &(mesh_i->facetype));
        DBAddOption(c_optlist, DBOPT_PLANAR, (void *) &(mesh_i->planar));
        DBAddOption(c_optlist, DBOPT_NSPACE, (void *) &(mesh_i->nspace));
        DBAddOption(c_optlist, DBOPT_ORIGIN, (void *) &(mesh_i->origin));
        DBAddOption(c_optlist, DBOPT_TIME, (void *) &(mesh_i->time));
        DBAddOption(c_optlist, DBOPT_DTIME, (void *) &(mesh_i->dtime));
        DBAddOption(c_optlist, DBOPT_XLABEL, (void *) &(mesh_i->xname));
        DBAddOption(c_optlist, DBOPT_YLABEL, (void *) &(mesh_i->yname));
        DBAddOption(c_optlist, DBOPT_ZLABEL, (void *) &(mesh_i->zname));
        DBAddOption(c_optlist, DBOPT_BASEINDEX, (void *) &(mesh_i->base_index));

        *optlist_id = DBFortranAllocPointer(c_optlist);
    }

    /*
     * Flush output buffer and set quadmesh_out
     */
    fflush(stdout);
    *quadmesh_out = *mesh_i;
}

/*
 * Read Quadvar. Called from Fortran
 */
void load_db_quadvar_c(int* dbid, char* name, int* lname, DBquadvar_intermediate* varout, int* optlist_id, int* err){
    int i;
    char* name_loc = f_to_c_str(name, lname);

    DBfile* file = (DBfile *) DBFortranAccessPointer (*dbid);
    DBquadvar_intermediate* var_i = (DBquadvar_intermediate*) malloc(sizeof(DBquadvar_intermediate));
    DBquadvar* var = DBGetQuadvar(file, name_loc);

    /* Check for error in DBGetQuadvar */
    if (!var){
      *err=-1;
      return;
    }

    /*
     * Allocate intermediate arrays
     */
    var_i->align = (double*) malloc(sizeof(double) * 6);

    /*
     * Convert DBquadvar to intermediate struct
     */
    var_i->id = var->id;
    var_i->name = var->name;
    var_i->units = var->units;
    var_i->label = var->label;
    var_i->cycle = var->cycle;
    var_i->meshid = var->meshid;
    var_i->vals = (void*)(((float**)(var->vals))[0]);       // ((double**)(readmesh->coords))[0][0])
    var_i->datatype = var->datatype;
    var_i->nels = var->nels;
    var_i->nvals = var->nvals;
    var_i->ndims = var->ndims;
    var_i->dims = var->dims;
    var_i->major_order = var->major_order;
    var_i->stride = var->stride;
    var_i->min_index = var->min_index;
    var_i->max_index = var->max_index;
    var_i->origin = var->origin;
    var_i->time = var->time;
    var_i->dtime = var->dtime;
    for(i = 0; i < 6; ++i){
        var_i->align[i] = var->align[i];
    }
    // TODO: Mixvals
    var_i->mixlen = var->mixlen;
    var_i->use_specmf = var->use_specmf;
    var_i->ascii_labels = var->ascii_labels;
    var_i->meshname = var->meshname;
    var_i->guihide = var->guihide;
    var_i ->conserved = var->conserved;
    var_i->extensive = var->extensive;
    var_i->centering = var->centering;
    var_i->missing_value = var->missing_value;


    if (*optlist_id != -99) {
        DBoptlist *c_optlist = DBMakeOptlist(MAX_OPTS);

        DBAddOption(c_optlist, DBOPT_CYCLE, (void *) &(var_i->cycle));
        DBAddOption(c_optlist, DBOPT_MAJORORDER, (void *) &(var_i->major_order));
        DBAddOption(c_optlist, DBOPT_ORIGIN, (void *) &(var_i->origin));
        DBAddOption(c_optlist, DBOPT_TIME, (void *) &(var_i->time));
        DBAddOption(c_optlist, DBOPT_DTIME, (void *) &(var_i->dtime));
        DBAddOption(c_optlist, DBOPT_ASCII_LABEL, (void *) &(var_i->ascii_labels));
        DBAddOption(c_optlist, DBOPT_CONSERVED, (void *) &(var_i->conserved));
        DBAddOption(c_optlist, DBOPT_EXTENSIVE, (void *) &(var_i->extensive));
        DBAddOption(c_optlist, DBOPT_HIDE_FROM_GUI, (void *) &(var_i->guihide));

        *optlist_id = DBFortranAllocPointer(c_optlist);
    }

    /*
     * Flush output buffer and set quadvar_out
     */
    fflush(stdout);
    *varout = *var_i;
    *err = 0;
}

/*
 * Read Multimesh. Called from Fortran
 */
void load_db_multimesh_c(int* dbid, char* name, int* lname, DBmultimesh_intermediate* meshout, int* mm_optlist){
  
    char* name_loc = f_to_c_str(name, lname);

    DBfile* file = (DBfile *) DBFortranAccessPointer (*dbid);

    DBmultimesh_intermediate* mesh_i = (DBmultimesh_intermediate*) malloc(sizeof(DBmultimesh_intermediate));
    DBmultimesh* mesh = DBGetMultimesh(file, name_loc);

    /*
     * Convert DBmultimesh to intermediate struct
     */
    mesh_i->id = mesh->id;
    mesh_i->nblocks = mesh->nblocks;
    mesh_i->ngroups = mesh->ngroups;
    mesh_i->meshids = mesh->meshids;
    mesh_i->meshnames = mesh->meshnames;
    mesh_i->meshtypes = mesh->meshtypes;
    mesh_i->dirids = mesh->dirids;
    mesh_i->blockorigin = mesh->blockorigin;
    mesh_i->grouporigin = mesh->grouporigin;
    mesh_i->extentssize = mesh->extentssize;
    mesh_i->extents = mesh->extents;
    mesh_i->zonecounts = mesh->zonecounts;
    mesh_i->has_external_zones = mesh->has_external_zones;
    mesh_i->guihide = mesh->guihide;
    mesh_i->lgroupings = mesh->lgroupings;
    mesh_i->groupings = mesh->groupings;
    // TODO: Groupnames
    // TODO: Mrgtree
    mesh_i->tv_connectivity = mesh->tv_connectivity;
    mesh_i->disjoint_mode = mesh->disjoint_mode;
    mesh_i->topo_dim = mesh->topo_dim;
    // TODO: file_ns
    // TODO: block_ns
    mesh_i->block_type = mesh->block_type;
    mesh_i->empty_list = mesh->empty_list;
    mesh_i->empty_cnt = mesh->empty_cnt;
    mesh_i->repr_block_idx = mesh->repr_block_idx;
    // TODO: alt_nodenum_vars
    // TODO: alt_zonenum_vars
    // TODO: Meshnames_alloc
    
    /*
     * Optlist
     */
    if (*mm_optlist != -99) {
        DBoptlist *c_optlist = DBMakeOptlist(MAX_OPTS);

        // Some of these are listed as opssible opts in documentation but are
        // not members of the multimesh struct
        DBAddOption(c_optlist, DBOPT_BLOCKORIGIN, (void *) &(mesh_i->blockorigin));
        // Cycle
        // Time
        // Dtime
        DBAddOption(c_optlist, DBOPT_EXTENTS_SIZE, (void *) &(mesh_i->extentssize));
        DBAddOption(c_optlist, DBOPT_EXTENTS, (void *) &(mesh_i->extents));
        DBAddOption(c_optlist, DBOPT_ZONECOUNTS, (void *) &(mesh_i->zonecounts));
        DBAddOption(c_optlist, DBOPT_HAS_EXTERNAL_ZONES, (void *) &(mesh_i->has_external_zones));
        DBAddOption(c_optlist, DBOPT_HIDE_FROM_GUI, (void *) &(mesh_i->guihide));
        // TODO: Build MRGTREE name handling
        DBAddOption(c_optlist, DBOPT_TV_CONNECTIVITY, (void *) &(mesh_i->tv_connectivity));
        DBAddOption(c_optlist, DBOPT_DISJOINT_MODE, (void *) &(mesh_i->disjoint_mode));
        DBAddOption(c_optlist, DBOPT_TOPO_DIM, (void *) &(mesh_i->topo_dim));
        DBAddOption(c_optlist, DBOPT_MB_BLOCK_TYPE, (void *) &(mesh_i->block_type));
        // TODO: Handle File namescheme
        // TODO: Handle block namescheme
        DBAddOption(c_optlist, DBOPT_MB_EMPTY_LIST, (void *) &(mesh_i->empty_list));
        DBAddOption(c_optlist, DBOPT_MB_EMPTY_COUNT, (void *) &(mesh_i->empty_cnt));


        *mm_optlist = DBFortranAllocPointer(c_optlist);
    }

    *meshout = *mesh_i;
    fflush(stdout);
}

/*
 * Read Multivar. Called from Fortran
 */
void load_db_multivar_c(int* dbid, char* name, int* lname, DBmultivar_intermediate* varout, int* mv_optlist){
    char* name_loc = f_to_c_str(name, lname);

    DBfile* file = (DBfile *) DBFortranAccessPointer (*dbid);

    DBmultivar_intermediate* var_i = (DBmultivar_intermediate*) malloc(sizeof(DBmultivar_intermediate));
    DBmultivar* var = DBGetMultivar(file, name_loc);

    /*
     * Allocate strings
     */
    var_i->mmesh_name = allocate_string(var->mmesh_name);
    var_i->file_ns = allocate_string(var->file_ns);
    var_i->block_ns = allocate_string(var->block_ns);
    var_i->varnames_alloc = allocate_string(var->varnames_alloc);

    /*
     * Convert DBmultivar to intermediate struct
     */
    var_i->id = var->id;
    var_i->nvars = var->nvars;
    var_i->ngroups = var->ngroups;
    var_i->varnames = var->varnames;
    var_i->vartypes = var->vartypes;
    var_i->blockorigin = var->blockorigin;
    var_i->grouporigin = var->grouporigin;
    var_i->extentssize = var->extentssize;
    var_i->extents = var->extents;
    var_i->guihide = var->guihide;
    // TODO: Region pnames
    copy_string(var->mmesh_name, var_i->mmesh_name);
    var_i->tensor_rank = var->tensor_rank;
    var_i->conserved = var->conserved;
    var_i->extensive = var->extensive;
    copy_string(var->file_ns, var_i->file_ns);
    copy_string(var->file_ns, var_i->block_ns);
    var_i->block_type = var->block_type;
    var_i->empty_list = var->empty_list;
    var_i->empty_cnt = var->empty_cnt;
    var_i->repr_block_idx = var->repr_block_idx;
    var_i->missing_value = var->missing_value;
    copy_string(var->varnames_alloc, var_i->varnames_alloc);

    if (*mv_optlist != -99) {
        DBoptlist *c_optlist = DBMakeOptlist(MAX_OPTS);

        DBAddOption(c_optlist, DBOPT_BLOCKORIGIN, (void *) &(var_i->blockorigin));
//    DBAddOption(c_optlist, DBOPT_CYCLE, (void*) &(var_i->cycle));
//    DBAddOption(c_optlist, DBOPT_TIME, (void*) &(var_i->time));
//    DBAddOption(c_optlist, DBOPT_DTIME, (void*) &(var_i->dtime));
        DBAddOption(c_optlist, DBOPT_HIDE_FROM_GUI, (void *) &(var_i->guihide));
        DBAddOption(c_optlist, DBOPT_EXTENTS_SIZE, (void *) &(var_i->extentssize));
        DBAddOption(c_optlist, DBOPT_EXTENTS, (void *) &(var_i->extents));
        DBAddOption(c_optlist, DBOPT_MMESH_NAME, (void *) &(var_i->mmesh_name));
        DBAddOption(c_optlist, DBOPT_TENSOR_RANK, (void *) &(var_i->tensor_rank));
//    DBAddOption(c_optlist, DBOPT_REGION_PNAMES, (void*) &(var_i->regionpnames));
        DBAddOption(c_optlist, DBOPT_CONSERVED, (void *) &(var_i->conserved));
        DBAddOption(c_optlist, DBOPT_EXTENSIVE, (void *) &(var_i->extensive));
        DBAddOption(c_optlist, DBOPT_MB_BLOCK_TYPE, (void *) &(var_i->block_type));
        DBAddOption(c_optlist, DBOPT_MB_FILE_NS, (void *) &(var_i->file_ns));
        DBAddOption(c_optlist, DBOPT_MB_EMPTY_LIST, (void *) &(var_i->empty_list));
        DBAddOption(c_optlist, DBOPT_MB_EMPTY_COUNT, (void *) &(var_i->empty_cnt));
        DBAddOption(c_optlist, DBOPT_MISSING_VALUE, (void *) &(var_i->missing_value));

        *mv_optlist = DBFortranAllocPointer(c_optlist);
    }

    *varout = *var_i;
    fflush(stdout);
}

void get_opt_i_c(int* optlist, int* opt, int* val){
    DBoptlist* optlist_c = DBFortranAccessPointer(*optlist);

    *val = *(int*) DBGetOption(optlist_c, *opt);
}

void get_opt_i_arr_c(int* optlist, int* opt, int** val){
    DBoptlist* optlist_c = DBFortranAccessPointer(*optlist);

    *val = ((int**)DBGetOption(optlist_c, *opt))[0];

//    *val = (int*) DBGetOption(optlist_c, *opt);
}

void
\
 get_opt_r_c(int* optlist, int* opt, float *val){
    DBoptlist* optlist_c = DBFortranAccessPointer(*optlist);

    *val = *(float*) DBGetOption(optlist_c, *opt);
}

// (double*) DBGetOption(c_optlist, 280)
void get_opt_d_c(int* optlist, int* opt, double *val){
    DBoptlist* optlist_c = DBFortranAccessPointer(*optlist);

    *val = *(double*) DBGetOption(optlist_c, *opt);
}


void get_opt_d_arr_c(int* optlist, int* opt, double** val){
    DBoptlist* optlist_c = DBFortranAccessPointer(*optlist);
    
    *val = ((double**)DBGetOption(optlist_c, *opt))[0];
}

void get_opt_str_c(int* optlist, int* opt, char* val){
    DBoptlist* optlist_c = DBFortranAccessPointer(*optlist);

    *val = *(char*) DBGetOption(optlist_c, *opt);       // Is syntax the problem with retrieving option?

}

/*
 * Returns the total size of a given set of coordinates or values
 */
int total_size(int ndims, int* dims){
    int i;
    int sum = 0;
    for (i = 0; i < ndims; ++i) {
        sum += dims[i];
    }
    return sum;
}

char* f_to_c_str(char* f_str, int* len) {
    char* c_str = malloc(*len+1);
    strncpy(c_str, f_str, *len);
    c_str[*len] = '\0';
    return c_str;
}

void get_string_at_index(int index, char** array, char* string_out){
    string_out = array[index];
}
