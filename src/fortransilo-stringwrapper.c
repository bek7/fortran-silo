/*
 * String Handler wrapper for Fortran Silo
 */

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<math.h>

// Populate a single string pointer with a line from a string array
void get_str_at_index(char** string_out, char*** array, int* idx){
	*string_out = array[0][(*idx) - 1];
}

// Find the longest item in an array of strings
void get_longest_str(int* longest, int* items, char*** array){
	int longest_found = 0;
	int i;
	for(i = 0; i < *items; ++i){
		longest_found=fmax(longest_found, strlen(array[0][i]));
	}
	*longest = longest_found;
}
