# Fortran Silo

Fortran-Silo is a Fortran 90 library intended for reading HDF5 files created in
LLNL's Silo library.

## Requirements

* LLNL Silo
* HDF5
* SZip
* CMake 3.8+

## Compilation and Installation

In most cases, entering the fortran-silo directory and running `cmake; make; make install` should build and install
Fortran Silo without any issues. Fortran Silo installs to `/usr/local/` by default. If you want to change this, specify
`-D CMAKE_INSTALL_DIR=<Some Path Here>` to manually specify the install prefix. If you have several compilers
available on your system, you may manually specify them either using environment variables or via CMake arguments.

To specify compilers using variables, do the following, substituting in the path to each executable on your system:

* C: `export CC=/usr/local/bin/icc`
* C++: `export CXX=/usr/local/bin/icpc`
* Fortran: `export FC=/usr/local/bin/ifort`

To use CMake arguments, use the following: `-D CMAKE_C_COMPILER=###`, `-D CMAKE_CXX_COMPILER=###`,
and `-D CMAKE_Fortran_COMPILER=###`. It is not necessary to specify the full path to the compiler when using arguments.


## Usage

After installing Fortran Silo, you will need to include the line `use fortran-silo` in the top of your Fortran code.
You will also need to specify the directories that the .mod files are located, by default this is `/usr/local/include`,
but it will be the include directory inside whatever install prefix you specified.
To specify the include directories, use the `-module {path here}` argument for iFort and `-I {path here}` for GNU.

### Functions

#### dbgetqm

Retrieves data from a Quadmesh stored in a silo file in the same manner it is written in Fortran.
All arguments are intent(out) unless specified otherwise, therefore they should be un-initialized and
arrays should be un-allocated. Returns a `0` if successfully retrieved quadmesh.

##### Prototype:

`integer function dbgetqm_opt(dbid, name, xname, yname, zname, x_dp, y_dp, z_dp, dims, ndims,
datatype, coordtype, qm_optlist) result(err)`

##### Argument Breakdown:

| Arg           | Type                      | Purpose                                                               |
|:--------------|:--------------------------|:----------------------------------------------------------------------|
| dbid          | intever (in)              | Integer pointer for file                                              |
| name          | character, pointer, (in)  | Name of quadmesh to retrieve                                          |
| xname         | character, pointer        | Will alllocate and populate with x axis name                          |
| yname         | character, pointer        | Will allocate and populate with y axis name                           |
| zname         | character, pointer        | Will allocate and populate with z axis name                           |
| x_coords      | real8, pointer            | Will allocate and populate with x coordinates                         |
| y_coords      | real8, pointer            | Will allocate and populate with y coordinates                         |
| z_coords      | real8, pointer            | Will allocate and populate with z coordinates                         |
| dims          | integer, pointer          | Length of coordinate arrays (/x_len, y_len, z_len/)                   |
| ndims         | integer                   | Number of dimensions stored in array                                  |
| datatype      | integer                   | Datatype for quadmesh. Silo constant                                  |
| Coordtype     | integer                   | Coordinate system type for quadmesh. Silo constant                    |
| optlist       | integer, optional         | Integer to store a silo int. pointer to attached optlist              |


#### dbgetqv

Retrieves a Quadvar stored in a Silo file in the same style it is written in Fortran. All arguments are
intent(out) unless otherwise specified, therefore should be un-initialized or unallocated. Returns `0`
on success.

##### Prototype:
`integer function dbgetqv(dbid, name, mesh_name, nvars, varnames, vars,
dims, ndims, mixvar, mixlen, datatype, centering, qv_optlist) result(err)`
                  
##### Argument Breakdown:

| Arg           | Type                      | Purpose                                                               |
|:--------------|:--------------------------|:----------------------------------------------------------------------|
| dbid          | intever (in)              | Integer pointer for file                                              |
| name          | character, pointer, (in)  | Name of quadvar to retrieve                                           |
| mesh_name     | character, pointer        | Name of associated Quadmesh                                           |
| nvars         | integer                   | Number of sub-variables comprising this variable                      |
| varnames      | character(len=:), pointer | Array of strings containing names of sub-variables                    |
| vars          | real8, pointer            | Array of pointers to varibles                                         |
| dims          | integer, pointer          | Size of each dimension in vars array                                  |
| ndims         | integer                   | Number of dimensions in vars array                                    |
| mixvar        | real8, pointer            | Array of pointers to each mix data values.                            |
| datatype      | integer                   | Datatype. Silo Constant.                                              |
| centering     | integer                   | Centering of sub-variables. Silo Constant.                            |
| optlist       | integer, optional         | DBID for attached optlist.                                            |


#### dbgetmm

#### dbgetmv
